import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 */

/**
 * @author sukena.naoto
 *
 */
public class FizzBuzzTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * {@link FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。(Fizz)
	 */
	@Test
	public void CheckFizzBuzz_01() {
		assertEquals("Fizz", FizzBuzz.checkFizzBuzz(9));
	}

	/**
	 * {@link FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。(Buzz)
	 */
	@Test
	public void CheckFizzBuzz_02() {
		assertEquals("Buzz", FizzBuzz.checkFizzBuzz(20));
	}

	/**
	 * {@link FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。(FizzBuzz)
	 */
	@Test
	public void CheckFizzBuzz_03() {
		assertEquals("FizzBuzz", FizzBuzz.checkFizzBuzz(45));
	}

	/**
	 * {@link FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。("44")
	 */
	@Test
	public void CheckFizzBuzz_04() {
		assertEquals("44", FizzBuzz.checkFizzBuzz(44));
	}

	/**
	 * {@link FizzBuzz#checkFizzBuzz(int)} のためのテスト・メソッド。("46")
	 */
	@Test
	public void CheckFizzBuzz_05() {
		assertEquals("46", FizzBuzz.checkFizzBuzz(46));
	}

}
